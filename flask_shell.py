''' configures the shell by importing required instances '''
from app import app, db
from app.models import User, Post

@app.shell_context_processor
def make_shell_context():
    ''' pass imported objects to shell session'''
    return{"db": db, "User": User, "Post": Post}
