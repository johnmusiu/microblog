''' db structure outlined here'''
from time import time
from datetime import datetime
from hashlib import md5
import jwt
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from app import db, app, login

followers = db.Table('followers',
                     db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
                     db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
                    )

class User(UserMixin, db.Model):
    '''user table definition'''
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    about_me = db.Column(db.String(140))
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    posts = db.relationship('Post', backref='author', lazy='dynamic')
    followed = db.relationship(
        'User', secondary=followers,
        primaryjoin=(followers.c.follower_id == id),
        secondaryjoin=(followers.c.followed_id == id),
        backref=db.backref('followers', lazy='dynamic'), lazy='dynamic'
    )

    def __repr__(self):
        ''' defines how python prints objects of this class'''
        return '< User {} >'.format(self.username)

    def set_password(self, password):
        ''' hash password '''
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        ''' verify password '''
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        ''' produces unique avatar for users using email '''
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

    def follow(self, user):
        ''' method to create a follower '''
        if not self.is_following(user):
            self.followed.append(user)

    def unfollow(self, user):
        ''' method to delete a follower '''
        if self.is_following(user):
            self.followed.remove(user)

    def is_following(self, user):
        ''' method to check if a follow relationship exists '''
        return self.followed.filter(
            followers.c.followed_id == user.id).count() > 0

    def followed_posts(self):
        ''' get all followed posts for a user (including their own),
            order then by time starting with the latest '''
        followed = Post.query.join(
            followers, (followers.c.followed_id == Post.user_id)).filter(
                followers.c.follower_id == self.id)
        own = Post.query.filter_by(user_id=self.id)
        return followed.union(own).order_by(Post.timestamp.desc())

    def get_reset_password_token(self, expires_in=600):
        ''' generate password reset token '''
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'], algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        ''' method to verify that a token is valid '''
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

class Post(db.Model):
    ''' posts table definition '''
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        ''' defines how python prints objects of this class '''
        return '<Post {}>'.format(self.body)

@login.user_loader
def load_user(id):
    ''' gf '''
    return User.query.get(int(id))
