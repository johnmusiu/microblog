''' class def for sending emails '''
from threading import Thread
from flask import render_template
# from flask_mail import Message
from app import app#, mail
from app import requests

def send_async_email(app, sender, recipients, subject, text_body):
    ''' send email asynchronously, ie in the background '''
    with app.app_context():
        requests.post(
            app.config['MAILGUN_REQUEST_URL'], auth=('api', app.config['MAILGUN_KEY']),
            data={
                'from': sender,
                'to': recipients,
                'subject': subject,
                'text': text_body
            })

def send_email(subject, sender, recipients, text_body): #, html_body):
    ''' method to send emails '''
    # msg = Message(subject, sender=sender, recipients=recipients)
    # msg.body = text_body
    # msg.html = html_body
    # mail.send(msg)
    requests.post(
        app.config['MAILGUN_REQUEST_URL'], auth=('api', app.config['MAILGUN_KEY']),
        data={
            'from': sender,
            'to': recipients,
            'subject': subject,
            'text': text_body
        })
    # Thread(target=send_async_email, args=(app, sender, subject, recipients, text_body))

def send_password_reset_email(user):
    ''' send email reset instructions via email '''
    token = user.get_reset_password_token()
    send_email('[Microblog] Reset Your Password',
               sender=app.config['ADMINS'][0],
               recipients=[user.email],
               text_body=render_template('email/reset_password.txt',
                                         user=user, token=token))
            #    html_body=render_template('email/reset_password.html',
                                        #  user=user, token=token))
